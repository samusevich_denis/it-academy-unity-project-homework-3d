﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    [SerializeField] private Transform targetPoint;

    [SerializeField] private float spead = 45f;


    // Update is called once per frame
    void Update()
    {
        var point = targetPoint.InverseTransformPoint(transform.position);
        transform.position = targetPoint.TransformPoint(point.normalized*2);
        transform.RotateAround(targetPoint.position, targetPoint.up,spead*Time.deltaTime);
    }
}
