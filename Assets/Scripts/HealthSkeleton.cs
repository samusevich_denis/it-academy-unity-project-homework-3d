﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSkeleton : MonoBehaviour
{
    [SerializeField] private int health = 5;

    // Start is called before the first frame update
    public int GetHealthSkeleton()
    {
        return health;
    }
    public void SetDamage(int damage)
    {
        health -= damage;
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}
