﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Animator animator;

    private Rigidbody rig;
    private float maxSpeed = 5;
    private bool isSetDirection;
    private static readonly int Movement = Animator.StringToHash("Movement");
    // Start is called before the first frame update
    void Start()
    {
        rig = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Move(InputController.HorizontalAxis);
    }

    public void Move(float direction)
    {
        float rotateSpeed = 0f;
        if (Mathf.Abs(direction) > 0.1f)
        {
            if (direction > 0.1)
            {
                rotateSpeed = Vector3.Angle(transform.forward, Vector3.forward);
            }
            if (direction < -0.1)
            {
                rotateSpeed = Vector3.Angle(transform.forward, Vector3.back);
            }
            if (rotateSpeed < 5f)
            {
                animator.SetInteger(Movement, 1);
                transform.rotation = Quaternion.Euler(0f, 90 * direction - 90, 0f);
                transform.Translate(new Vector3(0f, 0f, 1 * direction * Time.deltaTime), Space.World);
                return;
            }
        }

        if (Mathf.Abs(direction)<0.1f)
        {
            if (transform.forward.z < -0.1)
            {
                direction = 1;
            }
            else if (transform.forward.z > 0.1)
            {
                direction = -1;
            }
            else
            {
                transform.rotation = Quaternion.Euler(0f, -90f, 0f);
                return;
            }
        }
        animator.SetInteger(Movement, 0);
        transform.Rotate(new Vector3(0f, 90 * direction * Time.deltaTime, 0f));
    }
}
