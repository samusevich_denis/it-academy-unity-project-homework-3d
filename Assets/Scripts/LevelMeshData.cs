﻿using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "MeshesData", menuName = "Levels/Data", order = 1)]
public class LevelMeshData : ScriptableObject
{


    [SerializeField] private SerializableMeshInfo[] meshInfos;

    public SerializableMeshInfo[] MeshInfos => meshInfos;

    public void SetupData(GameObject obj)
    {
        var data = new List<SerializableMeshInfo>();

        foreach (var rend in obj.transform.GetComponentsInChildren<MeshRenderer>())
        {
            var sharedMaterial = rend.sharedMaterial;
            var target = rend.gameObject;
            var sharedMesh = target.GetComponent<MeshFilter>().sharedMesh;
            var meshInfo = new SerializableMeshInfo(target.name, sharedMesh, sharedMaterial, target.transform);
            data.Add(meshInfo);
        }

        meshInfos = data.ToArray();
    }

}
