﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGold : MonoBehaviour
{
    [SerializeField] private GameObject goldObject;
    [SerializeField] private Text goldCount;
    [SerializeField] private AnimationCurve curve;

    void Start()
    {
        goldCount.text = "00000";
        GameController.Instance.GoldAction += SetGoldCount;
    }

    private void SetGoldCount()
    {
        goldCount.text = GameController.Instance.Gold.ToString("d5");
    }

    private void OnDestroy()
    {
        GameController.Instance.GoldAction -= SetGoldCount;
    }

    private void Update()
    {

        float speedRotate = curve.Evaluate(Time.time % 5f*0.2f);
        goldObject.transform.RotateAround(goldObject.transform.position, goldObject.transform.up, 360* speedRotate * Time.deltaTime);
    }
}
