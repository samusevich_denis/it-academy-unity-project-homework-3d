﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIStars : MonoBehaviour
{
    [SerializeField] private GameObject[] stars;
    [SerializeField] private AnimationCurve curve;
    private int count = 0;
    void Start()
    {
        GameController.Instance.StarAction += StartRotate;
        for (int i = 0; i < stars.Length*0.5; i++)
        {
            stars[i*2].SetActive(false);
            stars[i * 2 + 1].SetActive(true);
        }
    }

    private void StartRotate()
    {
        StartCoroutine(PickUpStar());
    }
    private IEnumerator PickUpStar()
    {

        var index = count;
        count += 1;
        if (!stars[index * 2].activeSelf)
        {
            float timer = Time.time + 3f;
            float timerSpeed = 0;
            while (Time.time < timer)
            {
                float speedRotate = curve.Evaluate(timerSpeed);
                stars[index * 2].transform.Rotate(new Vector3(0f, 900f* speedRotate * Time.deltaTime, 0f));
                stars[index * 2 + 1].transform.Rotate(new Vector3(0f, 900f* speedRotate * Time.deltaTime, 0f));
                timerSpeed += Time.deltaTime;
                yield return null;
            }
            stars[index * 2].SetActive(true);
            stars[index * 2 + 1].SetActive(false);
            timer = Time.time + 3f;
            while (Time.time < timer)
            {
                float speedRotate = curve.Evaluate(timerSpeed);
                stars[index * 2].transform.Rotate(new Vector3(0f, 900* speedRotate * Time.deltaTime, 0f));
                stars[index * 2 + 1].transform.Rotate(new Vector3(0f, 900 * speedRotate * Time.deltaTime, 0f));
                timerSpeed += Time.deltaTime;
                yield return null;
            }
            stars[index * 2].transform.localRotation = Quaternion.Euler(new Vector3(0f, 90, 0f));

        }


    }

    private void OnDestroy()
    {
        GameController.Instance.StarAction -= StartRotate;
    }
}
