﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.UIElements;

public enum CharacterState
{
    Idle,
    Move,
    Attack,
    Skill,
    Hit,
    Dead,
}


[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    public static Action GameStart;

    private CharacterState characterState;
    [Header("Main")]
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject[] ballPrefabs;
    [SerializeField] private Transform firePoint;
    [SerializeField] private int skillBallCount;
    [SerializeField] private float skillAngleBound = 45;
    [SerializeField] private ParticleSystem particleSystem;
    [SerializeField] private InputController inputController;
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private Transform[] pointsRoad;
    [SerializeField] private Transform deadShotPoint;


    [Header("Bonuses")]
    [SerializeField] private GameObject hat;


    private static readonly int AttackTrigger = Animator.StringToHash("Attack");
    private static readonly int SkillTrigger = Animator.StringToHash("Skill");
    private static readonly int Movement = Animator.StringToHash("Movement");
    private InputCommand command;
    private bool stopCoroutine;
    public void EffectEvent()
    {
        StartCoroutine(EffectSkill(1f));
    }

    private IEnumerator EffectSkill(float time)
    {
        particleSystem.Play();

        stopCoroutine = true;
        while (stopCoroutine)
        {
            particleSystem.startSize += Time.deltaTime*5;
            yield return null;
        }
        particleSystem.startSize = 1;
        particleSystem.Stop();
        yield return null;

    }

    public void AttackEvent()
    {
        var obj = ObjectsPool.Instance.GetObject(ballPrefabs[(int)command], firePoint.transform.position, Quaternion.identity);
        var rig = obj.GetComponent<Rigidbody>();
        if (rig)
        {
            rig.velocity = Vector3.zero;
            rig.AddForce(transform.forward * 5f, ForceMode.Impulse);
        }
    }
    public void WearHat()
    {
        hat.SetActive(true);
    }
    public void SkillEvent()
    {
        stopCoroutine = false;
        var angleStep = skillAngleBound * 2 / (skillBallCount - 1);
        for (int i = 0; i < skillBallCount; i++)
        {
            var y = skillAngleBound - i * angleStep;

            var rotation = Quaternion.Euler(0f, y+transform.rotation.eulerAngles.y, 0f);
            var obj = ObjectsPool.Instance.GetObject(ballPrefabs[0], firePoint.transform.position, rotation);


            var rig = obj.GetComponent<Rigidbody>();
            obj.transform.Translate(Vector3.forward * 0.5f);
            if (rig)
            {
                rig.velocity = Vector3.zero;
                rig.AddForce(obj.transform.forward * 5f, ForceMode.Impulse);
            }
        }
    }

    private void Reset()
    {
        animator = GetComponent<Animator>();
    }

    void Start()
    {
        particleSystem.Stop();
        characterState = CharacterState.Idle;
        InputController.OnInputAction += OnInputComand;
        for (int i = 0; i < pointsRoad.Length; i++)
        {
            pointsRoad[i].gameObject.SetActive(false);
        }
        
        StartCoroutine(MovementProcess(2f,3f));
        hat.SetActive(false);

        ObjectsPool.Instance.AddObjects(ballPrefabs[0], 10);
    }

    private IEnumerator MovementProcess(float time, float timeJump)
    {
        yield return new WaitForSeconds(2f);
        var pos = transform.position;
        var timer = Time.time + timeJump;
        float timeCurve = 0;
        while (Time.time < timer)
        {
            var newPos = pos;
            timeCurve += Time.deltaTime/ timeJump;
            float y = curve.Evaluate(timeCurve);

            newPos.z += timeCurve * 5;
            newPos.y += y;
            transform.position = newPos;
            yield return null;
        }
        characterState = CharacterState.Move;
        Skill();
        animator.SetInteger(Movement, 1);

        for (int i = 0; i < pointsRoad.Length; i++)
        {
            while (!isReached(i))
            {
                while (characterState != CharacterState.Move)
                {
                    yield return new WaitForSeconds(0.1f);
                }
                float direction=1f;
                var checkpoint = transform.InverseTransformPoint(pointsRoad[i].position);
                if (checkpoint.x < 0)
                {
                    direction = -1f;
                }

                var angle = Vector3.Angle(Vector3.forward, checkpoint);

                transform.Translate(Vector3.forward * Time.deltaTime);
                transform.Rotate(0f, angle * direction * Time.deltaTime, 0f);

                yield return null;
            }
        }
        animator.SetInteger(Movement, 0);
        characterState = CharacterState.Idle;
        var playerMove = GetComponent<PlayerMovement>();
        playerMove.enabled = true;
        GameStart?.Invoke();
    }

    private bool isReached(int index)
    {
        if (Mathf.Abs(transform.position.x-pointsRoad[index].position.x)>0.5)
        {
            return false;
        }
        if (Mathf.Abs(transform.position.z - pointsRoad[index].position.z) > 0.5)
        {
            return false;
        }
        return true;
    }

    private void OnDestroy()
    {
        InputController.OnInputAction -= OnInputComand;
    }

    private void OnInputComand(InputCommand inputCommand)
    {

        switch (inputCommand)
        {
            case InputCommand.FireYellow:
                command = InputCommand.FireYellow;
                Attack();
                break;
            case InputCommand.FireBlue:
                command = InputCommand.FireBlue;
                Attack();
                break;
            case InputCommand.FireRed:
                command = InputCommand.FireRed;
                Attack();
                break;
            case InputCommand.FireGreen:
                command = InputCommand.FireGreen;
                Attack();
                break;
            case InputCommand.Skill:
                Skill();
                break;
            case InputCommand.DeadShot:
                DeadShot();
                break;
            case InputCommand.SuperShot:
                SuperShot();
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(inputCommand));
        }
    }

    private void Attack()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        CharacterState nextState = characterState == CharacterState.Move? CharacterState.Move:CharacterState.Idle;
        animator.SetTrigger(AttackTrigger);
        characterState = CharacterState.Attack;
        DelayRun.Execute(delegate { characterState = nextState; }, 0.5f, gameObject);
    }

    private void Skill()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        CharacterState nextState = characterState == CharacterState.Move ? CharacterState.Move : CharacterState.Idle;
        animator.SetTrigger(SkillTrigger);
        characterState = CharacterState.Skill;
        inputController.StartCooldown();
        DelayRun.Execute(delegate { characterState = nextState; }, 1.8f, gameObject);
    }
    private void DeadShot()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        animator.SetTrigger(SkillTrigger);
        characterState = CharacterState.Skill;

        DelayRun.Execute(delegate { characterState = CharacterState.Idle; }, 1.65f, gameObject);

        DelayRun.Execute(delegate
        {
            var hit = new RaycastHit();
            if (!Physics.Raycast(deadShotPoint.position, deadShotPoint.forward, out hit))
            {
                return;
            }

            var health = hit.transform.GetComponent<Health>();
            if (health != null)
            {
                health.SetDamage(int.MaxValue);
            }
        }, 1.35f, gameObject);

    }

    private void SuperShot()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }

        animator.SetTrigger(SkillTrigger);
        characterState = CharacterState.Skill;

        DelayRun.Execute(delegate { characterState = CharacterState.Idle; }, 1.65f, gameObject);

        DelayRun.Execute(delegate
        {
            var hitRay = new RaycastHit();
            if (!Physics.Raycast(deadShotPoint.position, deadShotPoint.forward, out hitRay))
            {
                return;
            }

            var hits = Physics.RaycastAll(deadShotPoint.position, deadShotPoint.forward);
            foreach (var hit in hits)
            {
                var health = hit.transform.GetComponent<Health>();
                if (health != null)
                {
                    health.SetDamage(int.MaxValue);
                }
            }
        }, 1.35f, gameObject);



    }
}
