﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarBonus : Bonus
{
    protected override void PickUpBonus()
    {
        base.PickUpBonus();
        GameController.Instance.Star += 1;
    }


}
