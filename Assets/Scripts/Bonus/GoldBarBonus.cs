﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldBarBonus : Bonus
{
    [SerializeField] private GameObject[] goldPrefabs;
    private Coroutine goldInstantiate;


    protected override void OnMouseDown()
    {
        
    }

    protected override void OnTriggerEnter(Collider other)
    {

    }

    protected override void Update()
    {
        transform.Translate(Time.deltaTime * 1f * Vector3.up, Space.World);

        if (transform.position.y>5f)
        {
            if (goldInstantiate!=null)
            {
                return;
            }
            goldInstantiate = StartCoroutine(StarInstantiateGold());
        }
    }
    
    private IEnumerator StarInstantiateGold()
    {

        var ran = Random.Range(4, 8);
        float angle = 360 / ran;
        for (int i = 0; i < ran; i++)
        {
            var obj = Instantiate(goldPrefabs[Random.Range(0, goldPrefabs.Length)],transform.position,Quaternion.Euler(0f, angle*i,0f));
            obj.transform.Translate(obj.transform.forward, Space.World);
            var rig = obj.GetComponent<Rigidbody>();
            rig.isKinematic = false;
            rig.AddForce(obj.transform.forward*2, ForceMode.Impulse);
            yield return new WaitForSeconds(0.5f);
        }
        Destroy(this.gameObject);
    }
}
