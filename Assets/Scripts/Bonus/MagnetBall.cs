﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetBall : MonoBehaviour
{
    public Vector3 positionMagnet;
    public float timer;
    public Rigidbody rig;

    void FixedUpdate()
    {
        if (timer<0)
        {
            Destroy(this);
        }
        timer -= Time.deltaTime;
        var vector = transform.InverseTransformPoint(positionMagnet)*transform.localScale.x;
        vector.y = 0;
        float effect = (4f - vector.sqrMagnitude) * 0.125f;
        vector = vector.normalized;
        rig.velocity += vector* effect;
    }
}
