﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetBonus : Bonus
{
    [SerializeField]private float timer = 5f;
    protected override void Reset()
    {
        var rig = GetComponent<Rigidbody>();
        rig.isKinematic = true;

        var sphereCollider = GetComponent<SphereCollider>();
        sphereCollider.isTrigger = true;
        sphereCollider.radius = 2f;
    }

    // Update is called once per frame
    protected override void Update()
    {

        if (transform.position.y<2f)
        {
            var speed = Mathf.Lerp(transform.position.y, 5, 0.5f);
            transform.Translate(Time.deltaTime * speed * Vector3.up, Space.World);
        }
        if (timer > 0)
        {
            timer -= Time.deltaTime;
            return;
        }
        Destroy(this.gameObject);
    }

    protected override void OnMouseDown()
    {
        
    }

    protected override void OnTriggerEnter(Collider other)
    {
        var ball = other.GetComponent<Ball>();
        if (ball)
        {
            var magnet =ball.gameObject.AddComponent<MagnetBall>();
            magnet.rig = ball.gameObject.GetComponent<Rigidbody>();
            magnet.positionMagnet = transform.position;
            magnet.timer = timer;
        }
    }
    protected void OnTriggerExit(Collider other)
    {
        var ball = other.GetComponent<MagnetBall>();
        if (ball)
        {
            Destroy(ball);
        }
    }

}
