﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldFromGoldBar : Bonus
{
    private float timeDestroy;
    [SerializeField] private int gold = 1;
    // Start is called before the first frame update
    void Start()
    {
        timeDestroy = Time.time + 2.5f;
        
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (transform.position.y < 0.5f)
        {
            var rig = GetComponent<Rigidbody>();
            rig.isKinematic = true;
        }
        if (timeDestroy>Time.time)
        {
            return;
        }
        var vectorScale = Vector3.Lerp(transform.localScale, Vector3.zero, 0.03f);
        transform.localScale = vectorScale;
        if (transform.localScale.x<0.1)
        {
            Destroy(this.gameObject);
        }
    }

    protected override void PickUpBonus()
    {
        base.PickUpBonus();
        Debug.Log($"Added {gold}  gold");
        GameController.Instance.Gold += gold;
    }
}
