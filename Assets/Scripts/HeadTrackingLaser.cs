﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadTrackingLaser : MonoBehaviour
{
    [SerializeField] private Transform skeleton;
    [SerializeField] private SetupLaserRenderen rightEyes;
    [SerializeField] private SetupLaserRenderen leftEyes;
    private Vector3 fixAngle = new Vector3(180f, 0f, 90f);
    private Quaternion lastRotate;
    private List<GameObject> targets;
    private GameObject target;

    private Coroutine coroutineTarget;

    void Start()
    {
        lastRotate = transform.rotation;
        targets = new List<GameObject>();
    }


    void LateUpdate()
    {
        if (TryGetTarget(out bool isNewTarget))
        {
            if (isNewTarget)
            {
                StopTargetCoroutine();
                coroutineTarget = StartCoroutine(WaitLaserShot(target));
            }
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position);
            transform.Rotate(fixAngle);
            transform.rotation = Quaternion.Lerp(lastRotate, transform.rotation, 0.1f);
            lastRotate = transform.rotation;
            return;
        }
        StopTargetCoroutine();
        transform.rotation = Quaternion.Lerp(lastRotate, transform.rotation, 0.1f);
        lastRotate = transform.rotation;
    }
    private void StopTargetCoroutine()
    {
        if (coroutineTarget != null)
        {
            StopCoroutine(coroutineTarget);
            rightEyes.SetActiveLaser(false);
            leftEyes.SetActiveLaser(false);
        }
    }
    private IEnumerator WaitLaserShot(GameObject targetObject)
    {
        float timer = 1f;
        while (timer>0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        rightEyes.SetActiveLaser(true);
        leftEyes.SetActiveLaser(true);
        timer = 0.3f;
        while (timer>0)
        {
            timer -= Time.deltaTime;
            rightEyes.SetTargetLaser(targetObject.transform.position);
            leftEyes.SetTargetLaser(targetObject.transform.position);
            yield return null;

        }
        rightEyes.SetActiveLaser(false);
        leftEyes.SetActiveLaser(false);
        if (targetObject!=null&&target!=null)
        {
            if (target.GetInstanceID() == targetObject.GetInstanceID())
            {
                for (int i = 0; i < targets.Count; i++)
                {
                    if (targets[i].GetInstanceID() == targetObject.gameObject.GetInstanceID())
                    {
                        target = null;
                        targets.RemoveAt(i);
                    }
                }
                targetObject.SetActive(false);
                target = null;
            }
        }
        coroutineTarget = null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.TryGetComponent<Ball>(out var componentBall))
        {
            targets.Add(componentBall.gameObject);
            target = null;
            return;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.TryGetComponent<Ball>(out var componentBall))
        {
            for (int i = 0; i < targets.Count; i++)
            {
                if (targets[i].GetInstanceID() == componentBall.gameObject.GetInstanceID())
                {
                    if (target != null && target.GetInstanceID() == targets[i].GetInstanceID())
                    {
                        target = null;
                    }
                    targets.RemoveAt(i);
                }
            }
        }
    }
    private bool TryGetTarget(out bool isNewTarget)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i] == null || !targets[i].activeSelf)
            {
                targets.RemoveAt(i);
                i--;
            }
        }
        if (target!=null&&Vector3.Angle(Vector3.forward, skeleton.InverseTransformPoint(target.transform.position)) > 100f)
        {
            isNewTarget = false;
            return true;
        }
        for (int i = 0; i < targets.Count; i++)
        {
            if (Vector3.Angle(Vector3.forward, skeleton.InverseTransformPoint(targets[i].transform.position)) > 100f)
            {
                isNewTarget = true;
                target = targets[i];
                return true;
            }
        }
        target = null;
        isNewTarget = false;
        return false;
    }


}
