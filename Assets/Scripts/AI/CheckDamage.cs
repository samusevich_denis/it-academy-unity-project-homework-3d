﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ai
{
    public class CheckDamage : Node
    {
        [SerializeField] private HealthSkeleton skeleton;
        private int health;
        private bool isSkeletonDie;
        private void Start()
        {
            health = skeleton.GetHealthSkeleton();
        }

        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            if (isSkeletonDie)
            {
                return NodeState.Success;
            }
            if (health> skeleton.GetHealthSkeleton())
            {
                health = skeleton.GetHealthSkeleton();
                return NodeState.Success;
            }
            return NodeState.Failure;
        }
        public void SetSkeletonDie()
        {
            isSkeletonDie = true;
        }
    }
}

