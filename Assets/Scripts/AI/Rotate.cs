using UnityEngine;

namespace Ai
{
    public class Rotate : Node
    {
        [SerializeField] private Transform root;
        
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            root.transform.Rotate(0f,Random.Range(0f,360f),0f);
            return NodeState.Success;
        }
    }
}