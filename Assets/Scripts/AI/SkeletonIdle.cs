﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ai
{
    public class SkeletonIdle : Node
    {
        [SerializeField] private Animator animator;
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            animator.SetInteger("Movement", 0);
            return NodeState.Success;
        }
    }
}

