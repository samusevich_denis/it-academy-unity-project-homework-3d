using System;
using UnityEngine;

namespace Ai
{
    public class Seguence : Node
    {
        [SerializeField] private Node[] nodes;

        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            var anyChildRunning = false;
            foreach (var node in nodes)
            {
                switch (node.Evaluate())
                {
                    case NodeState.Success:
                        continue;
                    case NodeState.Failure:
                        NodeState = NodeState.Failure;
                        return NodeState;
                    case NodeState.Running:
                        anyChildRunning = true;
                        continue;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            NodeState = anyChildRunning ? NodeState.Running : NodeState.Success;
            return NodeState;
        }
    }
}