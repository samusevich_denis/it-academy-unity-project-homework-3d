﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ai
{
    public class CheckTime : Node
    {
        [SerializeField] private float timer;
        private float timeWait;
        private Coroutine coroutineTimer;

        public override NodeState Evaluate()
        {
            if (timeWait > Time.time)
            {
                return NodeState.Success;
            }
            if (coroutineTimer == null)
            {
                coroutineTimer = StartCoroutine(SetTimerWait());
            }
            return NodeState.Failure;

        }

        private IEnumerator SetTimerWait()
        {
            yield return new WaitForSeconds(10f);
            timeWait = Time.time + timer;
            coroutineTimer = null;
        }
    }
}

