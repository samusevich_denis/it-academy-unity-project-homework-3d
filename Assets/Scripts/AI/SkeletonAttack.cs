using System.Collections;
using UnityEngine;

namespace Ai
{
    public class SkeletonAttack : Node
    {
        [SerializeField] private Animator animator;
        private Coroutine attackProcessCoroutine;
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            if (attackProcessCoroutine!=null)
            {
                return NodeState.Running;
            }

            var player = FindObjectOfType<PlayerController>();
            if (player == null)
            {
                return NodeState.Failure;
            }

            if (Vector3.Distance(transform.position, player.transform.position)>1f)
            {
                return NodeState.Failure;
            }

            attackProcessCoroutine = StartCoroutine(AttackProcess());
            return NodeState.Success;
        }

        private IEnumerator AttackProcess()
        {
            animator.SetTrigger("Attack");
            yield return new WaitForSeconds(1f);
            attackProcessCoroutine = null;
        }
    }
}