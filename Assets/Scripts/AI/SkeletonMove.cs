using UnityEngine;

namespace Ai
{
    public class SkeletonMove : Node
    {
        [SerializeField] private Transform root;
        [SerializeField] private Animator animator;
        [SerializeField] private int speed = 1;
        
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            animator.SetInteger("Movement", speed);
            root.Translate(transform.forward*-1f*Time.deltaTime*speed,Space.World);
            return NodeState.Success;
        }
    }
}