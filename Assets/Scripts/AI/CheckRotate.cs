﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ai
{
    public class CheckRotate : Node
    {
        [SerializeField] private Transform forwardPoint;
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            if (Physics.Raycast(forwardPoint.position, forwardPoint.forward, out var hit, 4f))
            {
                if (hit.collider != null)
                {
                    var wall = hit.collider.GetComponent<WallCollider>();
                    if (wall != null)
                    {
                        return NodeState.Success;
                    }
                }
            }
            return NodeState.Failure;
        }
    }
}

