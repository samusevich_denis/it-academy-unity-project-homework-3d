using UnityEngine;

namespace Ai
{
    public class CheckOutOfField : Node
    {
        [SerializeField] private Transform root;
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            if (NodeState == NodeState.Failure)
            {
                return NodeState;
            }
            if (transform.position.z > 18f)
            {
                return NodeState.Running;
            }
            root.transform.Rotate(0f, 90f, 0f);
            NodeState = NodeState.Failure;
            return NodeState;
        }
    }
}