﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ai
{
    public class SkeletonHurt : Node
    {
        [SerializeField] private Animator animator;
        private Coroutine coroutineHurt;
        public override NodeState Evaluate()
        {
            if (coroutineHurt != null)
            {
                return NodeState.Running;
            }
            StartCoroutine(SkeletonDamage());
            return NodeState.Running;
        }

        private IEnumerator SkeletonDamage()
        {
            animator.SetTrigger("Hurt");
            //ToDO
            yield return null; 
        }
    }

}
