﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ai
{
    public class PickUpBonus : Node
    {
        [SerializeField] private Transform forwardPoint;
        [SerializeField] private WarCry nodeWarCry;
        [SerializeField] private FakeAttack nodeFakeAttack;
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            if (Physics.Raycast(forwardPoint.position,forwardPoint.forward, out var hit, 1f))
            {
                if (hit.collider!=null)
                {
                    var bonus = hit.collider.GetComponent<BonusContainer>();
                    if (bonus)
                    {
                        bonus.PickUpBonusForSkeleton();
                    }
                }
            }
            nodeFakeAttack.SetNodeState();
            nodeWarCry.SetStateWait();
            return NodeState.Failure;
        }
    }
}

