﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ai
{
    public class CheckPlayerPosition : Node
    {
        [SerializeField] private FindPlayer nodeFindPlayer;
        [SerializeField] private WarCry nodeWarCry;

        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);

            if (!nodeFindPlayer.isFindPlayer)
            {
                return NodeState.Failure;
            }
            if (Vector3.Distance(nodeFindPlayer.GetPlayerPosition(),transform.position)>1f)
            {
                return NodeState.Success;
            }
            nodeFindPlayer.SetNodeState();
            nodeWarCry.SetStateWait();
            return NodeState.Failure;
        }
    }
}

