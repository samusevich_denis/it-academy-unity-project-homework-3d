﻿using System.Collections;
using UnityEngine;

namespace Ai
{
    public class FakeAttack : Node
    {
        [SerializeField] private Animator animator;


        private Coroutine attackProcessCoroutine;
        public override NodeState Evaluate()
        {
            if (NodeState ==  NodeState.Failure)
            {
                return NodeState;
            }
            Debug.Log(this.gameObject.name);
            if (attackProcessCoroutine != null)
            {
                return NodeState.Running;
            }

            attackProcessCoroutine = StartCoroutine(AttackProcess());
            return NodeState.Success;
        }

        private IEnumerator AttackProcess()
        {
            animator.SetTrigger("Attack");
            yield return new WaitForSeconds(1f);
            attackProcessCoroutine = null;
            NodeState = NodeState.Failure;
        }

        public void SetNodeState()
        {
            NodeState = NodeState.Success;
        }
    }
}