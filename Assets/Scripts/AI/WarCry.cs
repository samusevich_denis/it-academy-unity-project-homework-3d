using System;
using System.Collections;
using UnityEngine;

namespace Ai
{
    public class WarCry : Node
    {
        [SerializeField] private Animator animator;
        private enum State
        {
            Wait,
            WarCry,
            Complete,
        }
        //ToDO
        private State state;
        private Coroutine warCryCoroutine;
        
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            switch (state)
            {
                case State.Wait:
                    state = State.WarCry;
                    warCryCoroutine = StartCoroutine(WarCryProcess());
                    return NodeState.Running;
                    

                case State.WarCry:
                    if (warCryCoroutine!=null)
                    {
                        return NodeState.Running;
                    }
                    else
                    {
                        state = State.Complete;
                        return NodeState.Success;
                    }
                case State.Complete:
                    return NodeState.Failure;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }


        private IEnumerator WarCryProcess()
        {
            animator.SetTrigger("WarCry");
            yield return new WaitForSeconds(1.5f);
            warCryCoroutine = null;
        }

        public void SetStateWait()
        {
            state = State.Wait;
        }
    }
}