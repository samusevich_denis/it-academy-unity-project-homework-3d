﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ai
{
    public class FindPlayer : Node
    {
        [SerializeField] private Transform forwardPoint;

        private Vector3 playerPosition;
        public bool isFindPlayer;
        
        private void Start()
        {
            isFindPlayer = false;
            NodeState = NodeState.Failure;
        }
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            if (NodeState == NodeState.Success)
            {
                //TODO
                return NodeState;
            }
            if (Physics.Raycast(forwardPoint.position, forwardPoint.forward, out var hit, 20f))
            {
                if (hit.collider != null)
                {
                    var player = hit.collider.GetComponent<PlayerController>();
                    if (player != null)
                    {
                        playerPosition = player.transform.position;
                        isFindPlayer = true;
                        NodeState = NodeState.Success;
                        return NodeState.Success;
                    }
                }
            }
            return NodeState.Failure;
        }

        public Vector3 GetPlayerPosition()
        {
            return playerPosition;
        }

        public void SetNodeState()
        {
            isFindPlayer = false;
            NodeState = NodeState.Failure;
        }
    }
}

