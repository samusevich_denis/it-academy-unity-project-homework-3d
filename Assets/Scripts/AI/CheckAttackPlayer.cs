using System;

namespace Ai
{
    public class CheckAttackPlayer : Node
    {
        public bool isCanRotate;
        
        public override NodeState Evaluate()
        {
            var node = isCanRotate ? NodeState.Running : NodeState.Failure;
            isCanRotate = false;

            return node;
        }
    }
}