using UnityEngine;

namespace Ai
{
    public class CheckChest : Node
    {
        [SerializeField] private Transform forwardPoint;
        public override NodeState Evaluate()
        {

            Debug.Log(this.gameObject.name);
            if (Physics.Raycast(forwardPoint.position,forwardPoint.forward, out var hit, 1f))
            {
                if (hit.collider != null)
                {
                    var bonus = hit.collider.GetComponent<BonusContainer>();
                    return bonus != null ? NodeState.Success : NodeState.Failure;
                }
            }

            return NodeState.Failure;
        }
    }
}