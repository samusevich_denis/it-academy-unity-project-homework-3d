﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ai
{
    public class SkeletonDie : Node
    {
        [SerializeField] private HealthSkeleton skeleton;
        [SerializeField] private Animator animator;
        [SerializeField] private CheckDamage nodeCheckDamage;

        private Coroutine coroutineDie;
        public override NodeState Evaluate()
        {
            Debug.Log(this.gameObject.name);
            if (coroutineDie != null)
            {
                return NodeState.Running;
            }
            if (skeleton.GetHealthSkeleton()<=0)
            {
                coroutineDie = StartCoroutine(DieSkeleton());
                return NodeState.Running;
            }
            return NodeState.Failure;
        }

        private IEnumerator DieSkeleton()
        {
            animator.SetTrigger("Die");
            nodeCheckDamage.SetSkeletonDie();
            yield return new WaitForSeconds(5f);
            skeleton.Die();

        }
    }
}

