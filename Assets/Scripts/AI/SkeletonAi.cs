﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

namespace Ai
{
    public class SkeletonAi : MonoBehaviour
    {
        [SerializeField] private Node rootNode;

        // Update is called once per frame
        void Update()
        {
            rootNode.Evaluate();
        }
    }
}