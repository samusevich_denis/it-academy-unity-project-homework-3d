﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadTracking : MonoBehaviour
{

    [SerializeField] private Transform skeleton;

    private Vector3 fixAngle = new Vector3(180f, 0f, 90f);
    private GameObject targetDefault;
    private Quaternion lastRotate;
    private List<GameObject> targets;
    private GameObject target;

    void Start()
    {

        targetDefault = FindObjectOfType<PlayerController>().gameObject;

        lastRotate = transform.rotation;
        targets = new List<GameObject>();
    }


    void LateUpdate()
    {
        if (TryGetTarget(out target))
        {
            transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position);
            transform.Rotate(fixAngle);
            transform.rotation = Quaternion.Lerp(lastRotate, transform.rotation, 0.1f);
            lastRotate = transform.rotation;
            return;
        }
        transform.rotation = Quaternion.Lerp(lastRotate, transform.rotation, 0.1f);
        lastRotate = transform.rotation;
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.TryGetComponent<Rigidbody>(out var component))
        {
            targets.Add(component.gameObject);
            target = null;
            return;
        }
    }
    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.TryGetComponent<Rigidbody>(out var component))
        {
            for (int i = 0; i < targets.Count; i++)
            {
                if (targets[i].GetInstanceID() == component.gameObject.GetInstanceID())
                {
                    if (target!=null && target.GetInstanceID() == targets[i].GetInstanceID())
                    {
                        target = null;
                    }
                    targets.RemoveAt(i);
                }
            }
        }
    }
    private bool TryGetTarget(out GameObject target)
    {
        for (int i = 0; i < targets.Count; i++)
        {
            if (targets[i]==null)
            {
                targets.RemoveAt(i);
                i--;

            }
            if (Vector3.Angle(Vector3.forward, skeleton.InverseTransformPoint(targets[i].transform.position)) > 100f)
            {
                target = targets[i];
                return true;
            }
        }

        if (Vector3.Angle(Vector3.forward, skeleton.InverseTransformPoint(targetDefault.transform.position)) > 100f)
        {
            target = targetDefault;
            return true;
        }

        target = null;
        return false;
    }
}
