﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxEmergence : MonoBehaviour
{
    private void Start()
    {
        PlayerController.GameStart += StartEmergence;
    }

    private void StartEmergence()
    {
        StartCoroutine(Emergence());
    }
    private IEnumerator Emergence()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var transformChild = transform.GetChild(i);
            var rig = transformChild.GetComponent<Rigidbody>();
            rig.isKinematic = false;
            yield return new WaitForSeconds(0.08f);
        }
    }
    private void OnDestroy()
    {
        PlayerController.GameStart -= StartEmergence;
    }
}
