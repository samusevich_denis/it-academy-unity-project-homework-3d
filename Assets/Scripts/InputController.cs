﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum InputCommand
{
    FireYellow,
    FireBlue,
    FireRed,
    FireGreen,
    Skill,
    DeadShot,
    SuperShot,
}


public class InputController : MonoBehaviour
{
    public static Action<InputCommand> OnInputAction;

    public static float HorizontalAxis { get; private set; }

    [SerializeField] private Button fireYellowButton;
    [SerializeField] private Button fireBlueButton;
    [SerializeField] private Button fireRedButton;
    [SerializeField] private Button fireGreenButton;
    [SerializeField] private Button skillButton;
    [SerializeField] private Button deadShotButton;
    [SerializeField] private Button superShotButton;
    [SerializeField] private Image image;



    private Coroutine coroutine;


    public void StartCooldown()
    {
        if (coroutine != null)
        {
            return;
        }
        coroutine = StartCoroutine(CooldownSkiil(1.8f));
    }

    private void Awake()
    {
        fireYellowButton.onClick.AddListener(OnFireYellowButton);
        fireBlueButton.onClick.AddListener(OnFireBlueButton);
        fireRedButton.onClick.AddListener(OnFireRedButton);
        fireGreenButton.onClick.AddListener(OnFireGreenButton);
        skillButton.onClick.AddListener(OnSkillButton);
        deadShotButton.onClick.AddListener(OnDeadShotButton);
        superShotButton.onClick.AddListener(OnSuperShotButton);
    }

    private void OnFireYellowButton()
    {
        OnInputAction?.Invoke(InputCommand.FireYellow);
    }

    private void OnFireBlueButton()
    {
        OnInputAction?.Invoke(InputCommand.FireBlue);
    }

    private void OnFireRedButton()
    {
        OnInputAction?.Invoke(InputCommand.FireRed);
    }

    private void OnFireGreenButton()
    {
        OnInputAction?.Invoke(InputCommand.FireGreen);
    }

    private void OnSkillButton()
    {
        OnInputAction?.Invoke(InputCommand.Skill);
    }

    private void OnDeadShotButton()
    {
        OnInputAction?.Invoke(InputCommand.DeadShot);
    }
    private void OnSuperShotButton()
    {
        OnInputAction?.Invoke(InputCommand.SuperShot);
    }
    private IEnumerator CooldownSkiil(float time)
    {
        image.fillAmount = 0;
        image.color = Color.red;
        var step = 1 / time;
        while (image.fillAmount < 1)
        {
            var color = image.color;
            color.r -= step * Time.deltaTime;
            color.g += step * Time.deltaTime;
            image.fillAmount += step * Time.deltaTime;
            image.color = color;
            yield return null;
        }
        image.fillAmount = 1f;
        image.color = Color.green;
        coroutine = null;

    }

    private void Start()
    {
        HorizontalAxis = 0f;
    }

    private void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal");
    }
}
