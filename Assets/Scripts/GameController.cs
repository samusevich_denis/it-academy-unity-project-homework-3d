using System;
using UnityEngine;


public class GameController : SingletonObject<GameController>
{
    public Action StarAction;
    public Action GoldAction;

    private int gold;
    public int Gold
    {
        get => gold;
        set
        {
            gold = value;
            GoldAction?.Invoke();
        }
    }
    private  int starCount;
    public  int Star
    {
        get => starCount;
        set
        {
            starCount = value;
            StarAction?.Invoke();
        }
    }

}
