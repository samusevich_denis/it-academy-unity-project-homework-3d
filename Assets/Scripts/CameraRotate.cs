﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private Transform pointRotate;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Quaternion rotateLook = transform.rotation;

        rotateLook.SetLookRotation(pointRotate.InverseTransformPoint(player.position));


        var rotate = Quaternion.Lerp(transform.rotation, rotateLook, 0.4f);
        transform.rotation = rotate;
    }
}
