﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WindowLog : EditorWindow
{
    private static string Text;
    private static MethodData methodData;
    public static void ShowWindow(string text, MethodData method)
    {
        Text = text;
        methodData = method;

        EditorWindow.GetWindow(typeof(WindowLog));
    }
    private void OnGUI()
    {
        title = "Warning!!!!";
        minSize = new Vector2(500, 50);
        maxSize = new Vector2(500, 50);
        GUILayout.Label(Text);
        GUILayout.BeginHorizontal();
        GUILayout.Space(200);
        if (GUILayout.Button("Yes", GUILayout.Width(50)))
        {
            methodData.Invoke();
            Close();
        }

        if (GUILayout.Button("No", GUILayout.Width(50)))
        {
            Close();
        }
        GUILayout.EndHorizontal();
    }
}
