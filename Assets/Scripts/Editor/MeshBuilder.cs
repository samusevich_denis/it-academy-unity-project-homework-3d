﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class MeshBuilder : EditorWindow
{
    [MenuItem("Tools/MeshBuilder/Add noise to mesh")]
    private static void AddNoize()
    {
        var obj = Selection.activeGameObject;
        if (obj == null)
        {
            Debug.LogError("No object in selection");
            return;
        }
        
        var meshFilter = obj.GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            Debug.Log("No mesh filter in selection");
            return;
        }

        var vertices = meshFilter.sharedMesh.vertices;
        for (int i = 0; i < vertices.Length; i++)
        {
            var pos = vertices[i];
            pos.x += Random.Range(-0.1f, 0.1f);
            pos.y += Random.Range(-0.1f, 0.1f);
            pos.z += Random.Range(-0.1f, 0.1f);
            vertices[i] = pos;
        }

        meshFilter.sharedMesh.vertices = vertices;
    }

    [MenuItem("Tools/MeshBuilder/Deformation mesh")]
    private static void Deformation()
    {
        var obj = Selection.activeGameObject;
        if (obj == null)
        {
            Debug.LogError("No object in selection");
            return;
        }

        var meshFilter = obj.GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            Debug.Log("No mesh filter in selection");
            return;
        }

        var vertices = meshFilter.sharedMesh.vertices;

        List<List<int>> indexVertices = new List<List<int>>();

        bool flagNewVertices = true;
        for (int i = 0; i < vertices.Length; i++)
        {
            flagNewVertices = true;
            for (int j = 0; j < indexVertices.Count; j++)
            {

                if (EqualsVector3(vertices[i], vertices[indexVertices[j][0]] ))
                {
                    flagNewVertices = false;
                    indexVertices[j].Add(i);
                }
            }
            if (flagNewVertices)
            {
                indexVertices.Add(new List<int>());
                indexVertices[indexVertices.Count - 1].Add(i);
            }
        }
        for (int i = 0; i < indexVertices.Count; i++)
        {
            float random = Random.Range(-0.1f, 0.1f);
            for (int j = 0; j < indexVertices[i].Count; j++)
            {
                vertices[indexVertices[i][j]].x += random;
                vertices[indexVertices[i][j]].y += random;
                vertices[indexVertices[i][j]].z += random;
            }
        }
        Debug.Log(indexVertices.ToString());
        meshFilter.sharedMesh.vertices = vertices;
    }

    [MenuItem("Tools/MeshBuilder/Deformation Up vector mesh")]
    private static void DeformationUp()
    {
        var obj = Selection.activeGameObject;
        if (obj == null)
        {
            Debug.LogError("No object in selection");
            return;
        }

        var meshFilter = obj.GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            Debug.Log("No mesh filter in selection");
            return;
        }

        var vertices = meshFilter.sharedMesh.vertices;

        List<List<int>> indexVertices = new List<List<int>>();

        bool flagNewVertices = true;
        for (int i = 0; i < vertices.Length; i++)
        {
            flagNewVertices = true;
            for (int j = 0; j < indexVertices.Count; j++)
            {

                if (EqualsVector3(vertices[i], vertices[indexVertices[j][0]]))
                {
                    flagNewVertices = false;
                    indexVertices[j].Add(i);
                }
            }
            if (flagNewVertices)
            {
                indexVertices.Add(new List<int>());
                indexVertices[indexVertices.Count - 1].Add(i);
            }
        }
        float posMinY = vertices[indexVertices[0][0]].y;
        for (int i = 0; i < indexVertices.Count; i++)
        {
            if (vertices[indexVertices[i][0]].y <posMinY)
            {
                posMinY = vertices[indexVertices[i][0]].y;
            }
        }
        for (int i = 0; i < indexVertices.Count; i++)
        {
            for (int j = 0; j < indexVertices[i].Count; j++)
            {
                vertices[indexVertices[i][j]].y = vertices[indexVertices[i][j]].y+(vertices[indexVertices[i][j]].y- posMinY);
            }
        }
        Debug.Log(indexVertices.ToString());
        meshFilter.sharedMesh.vertices = vertices;
    }


    [MenuItem("Tools/MeshBuilder/Deformation rotate vector mesh")]
    private static void DeformationRotate()
    {
        var obj = Selection.activeGameObject;
        if (obj == null)
        {
            Debug.LogError("No object in selection");
            return;
        }

        var meshFilter = obj.GetComponent<MeshFilter>();
        if (meshFilter == null)
        {
            Debug.Log("No mesh filter in selection");
            return;
        }

        var vertices = meshFilter.sharedMesh.vertices;

        List<List<int>> indexVertices = new List<List<int>>();

        bool flagNewVertices = true;
        for (int i = 0; i < vertices.Length; i++)
        {
            flagNewVertices = true;
            for (int j = 0; j < indexVertices.Count; j++)
            {

                if (EqualsVector3(vertices[i], vertices[indexVertices[j][0]]))
                {
                    flagNewVertices = false;
                    indexVertices[j].Add(i);
                }
            }
            if (flagNewVertices)
            {
                indexVertices.Add(new List<int>());
                indexVertices[indexVertices.Count - 1].Add(i);
            }
        }
        float posMinY = vertices[indexVertices[0][0]].y;
        for (int i = 0; i < indexVertices.Count; i++)
        {
            if (vertices[indexVertices[i][0]].y < posMinY)
            {
                posMinY = vertices[indexVertices[i][0]].y;
            }
        }
        float posMaxY = vertices[indexVertices[0][0]].y;
        for (int i = 0; i < indexVertices.Count; i++)
        {
            if (vertices[indexVertices[i][0]].y > posMaxY)
            {
                posMaxY = vertices[indexVertices[i][0]].y;
            }
        }
        for (int i = 0; i < indexVertices.Count; i++)
        {
            for (int j = 0; j < indexVertices[i].Count; j++)
            {
                
                vertices[indexVertices[i][j]] = RotateVector(vertices[indexVertices[i][j]],(Mathf.PI*(vertices[indexVertices[i][j]].y- posMinY)/(posMaxY-posMinY)),obj.transform);

            }
        }

        meshFilter.sharedMesh.vertices = vertices;
    }

    private static bool EqualsVector3(Vector3 vectorOne, Vector3 vectorTwo)
    {
        if (Mathf.Abs(vectorOne.x - vectorTwo.x) > 0.0001)
        {
            return false;
        }
        if (Mathf.Abs(vectorOne.y - vectorTwo.y) > 0.0001)
        {
            return false;
        }
        if (Mathf.Abs(vectorOne.z - vectorTwo.z) > 0.0001)
        {
            return false;
        }
        return true;
    }

    private static Vector3 RotateVector(Vector3 positionWorldSpace, float radian, Transform rotatePoint)
    {
        var posY = positionWorldSpace.y;
        Vector2 localPositionXZ = new Vector2(positionWorldSpace.x, positionWorldSpace.z);
        var startRadian = Mathf.Acos(localPositionXZ.x / localPositionXZ.magnitude);
        var targetRadian = startRadian + radian;
        Vector3 localRotatePositionMax = new Vector3(localPositionXZ.magnitude * Mathf.Cos(targetRadian), positionWorldSpace.y, localPositionXZ.magnitude * Mathf.Sin(targetRadian));
        return localRotatePositionMax;
    }

    [MenuItem("Tools/MeshBuilder/Generate Line")]
    private static void GenerateLine()
    {
        var obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        var meshFilter = obj.GetComponent<MeshFilter>();

        int length = 5;
        
        var vertices = new List<Vector3>();
        var triangles = new List<int>();

        var index = 0;
        
        for (int x = 0; x < length; x++)
        {
            for (int y = 0; y < 2; y++)
            {
                index++;
                vertices.Add( new Vector3(x,y,0));
                if (x==0 || x== length-1)
                {
                    continue;
                }

                if (y==0)
                {
                    triangles.Add(index-2);
                    triangles.Add(index-1);
                    triangles.Add(index);
                }
                if (y==1)
                {
                    triangles.Add(index-2);
                    triangles.Add(index);
                    triangles.Add(index-1);
                }
                
            }
        }
        
        vertices.Add(Vector3.zero);
        vertices.Add(new Vector3(0,1,0));
        vertices.Add(new Vector3(1,0,0));
        
        triangles.Add(0);
        triangles.Add(1);
        triangles.Add(2);

        var mesh = new Mesh
        {
            vertices = vertices.ToArray(),
            triangles = triangles.ToArray()
        };
        
        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        meshFilter.sharedMesh = mesh;
        

    }
}
