﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LevelGenerator : EditorWindow
{
    private List<GameObject> prefabsFloor;
    private string[] namePrefabsFloor = new string[4] { "Levels_BaseObj_01", "Levels_BaseObj_002", "Levels_BaseObj_003", "Levels_BaseObj_004" };
    private List<GameObject> prefabsСorner;
    private string[] namePrefabsСorner = new string[1] { "groupedPieces012" };

    private List<GameObject> prefabsWallSmall;
    private string[] namePrefabsWallSmall = new string[2] { "Object001", "Object002"};
    private List<GameObject> prefabsWallMedium;
    private string[] namePrefabsWallMedium = new string[3] { "groupedPieces05", "groupedPieces009", "groupedPieces010" };
    private List<GameObject> prefabsWallBig;
    private string[] namePrefabsWallBig = new string[2] { "groupedPieces01", "groupedPieces011" };
    private List<GameObject> prefabGate;
    private string[] namePrefabGate = new string[1] { "pillar02_Arch" };

    private GameObject parentLevel;

    private int width;
    private int length;

    private float stepWallSmall = 3.5f;
    private float stepWallMedium = 4f;
    private float stepWallBig = 5.5f;
    private float stepGate = 5.5f;
    private float stepCornerSmall = 2.75f;
    private float stepCornerBig = 4.5f;
    private float stepCornerOffset = 2.25f;
    private readonly float stepFloor = 4;
    int id = 0;
    private Vector3 startPosition;

    private string projectObjectPath = "LevelObjects";
    private string stringWidth;
    private string stringLength;
    private Vector2 scrolPosition=Vector2.zero;
    private string textError = string.Empty;

    [MenuItem("Tools/Level Generator")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(LevelGenerator)).Show();
    }

    private void Awake()
    {
        var objs = Resources.LoadAll(projectObjectPath);

        FindPrefabsfromResources(objs, namePrefabsFloor,ref prefabsFloor);
        FindPrefabsfromResources(objs, namePrefabsСorner,ref prefabsСorner);
        FindPrefabsfromResources(objs, namePrefabsWallSmall,ref prefabsWallSmall);
        FindPrefabsfromResources(objs, namePrefabsWallMedium,ref prefabsWallMedium);
        FindPrefabsfromResources(objs, namePrefabsWallBig,ref prefabsWallBig);
        FindPrefabsfromResources(objs, namePrefabGate, ref prefabGate);
    }

    private void FindPrefabsfromResources(Object[] objs,string[] namesPrefabs, ref List<GameObject> prefabs)
    {
        prefabs = new List<GameObject>();
        for (int i = 0; i < objs.Length; i++)
        {
            for (int j = 0; j < namesPrefabs.Length; j++)
            {
                if (objs[i].name.Equals(namesPrefabs[j], System.StringComparison.Ordinal))
                {
                    prefabs.Add((GameObject)objs[i]);
                }
            }
        }
    }

    private void OnGUI()
    {
        scrolPosition =GUILayout.BeginScrollView(scrolPosition, GUIStyle.none);
        GUILayout.BeginHorizontal();
        GUILayout.Label("Width level -");
        stringWidth = GUILayout.TextField(stringWidth, GUILayout.Width(50));
        GUILayout.Label("Length level -");
        stringLength = GUILayout.TextField(stringLength, GUILayout.Width(50));
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Generate level"))
        {
            if (int.TryParse(stringWidth, out int numW)&& int.TryParse(stringLength, out int numL))
            {
                width = numW;
                length = numL;
                textError = string.Empty;
                if (parentLevel != null)
                {
                    DestroyImmediate(parentLevel);
                }
                GenerateLevel();
            }
            else
            {
                textError = "Invalid value";
            }
        }

        if (GUILayout.Button("Delete level"))
        {
            if (parentLevel != null)
            {
                DestroyImmediate(parentLevel);
            }

        }
        GUILayout.Label(textError);
        GUILayout.EndScrollView();
    }

    private void GenerateLevel()
    {
        startPosition = new Vector3(stepFloor * 0.5f, 0f, stepFloor * 0.5f);
        parentLevel = new GameObject("Level");
        var objFloor = new GameObject("Floor");
        objFloor.transform.parent = parentLevel.transform;
        var instantiatePosition = startPosition;

        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < length; j++)
            {
                var random = Random.Range(0, prefabsFloor.Count);
                var obj = Instantiate(prefabsFloor[random], instantiatePosition, Quaternion.identity, objFloor.transform);
                id++;
                obj.name = obj.name.Replace("(Clone)", "") + " " + id;
                instantiatePosition.z += stepFloor;
            }
            instantiatePosition.x += stepFloor;
            instantiatePosition.z = startPosition.z;
        }

        var objСorner = new GameObject("Сorner");
        objСorner.transform.parent = parentLevel.transform;

        var positionsAndScaleCorner = GetCornerPositionsAndScale();
        for (int i = 0; i < 4; i++)
        {
            var randomCorner = Random.Range(0, prefabsСorner.Count);
            var objCorner = Instantiate(prefabsСorner[randomCorner], positionsAndScaleCorner[i], Quaternion.identity, objСorner.transform);
            objCorner.name = objCorner.name.Replace("(Clone)", "") + " " + id;
            id++;
            objCorner.transform.localScale = positionsAndScaleCorner[i + 4];
        }


        CreateWall(false, "WallOne", parentLevel.transform, width, stepCornerSmall, new Vector3(stepCornerSmall, 0f, 0f), false);
        CreateWall(true, "WallTwo", parentLevel.transform, width, stepCornerSmall, new Vector3(stepCornerSmall, 0f, length * stepFloor), false);
        CreateWall(false, "WallThree", parentLevel.transform, length, stepCornerBig, new Vector3(0f, 0f, stepCornerBig), true);
        CreateWall(false, "WallFour", parentLevel.transform, length, stepCornerBig, new Vector3(width * stepFloor, 0f, stepCornerBig), true);
    }

    private void CreateWall(bool isGate, string name, Transform parent, float length, float stepCorner, Vector3 startPosition, bool isRotate)
    {
        var objWall = new GameObject(name);
        objWall.transform.parent = parent.transform;
        List<float> stepWalls = new List<float>();

        var wallObjs = GetGameObjects(isGate, length * stepFloor - stepCorner * 2, out stepWalls);
        var startPositionWall = startPosition;

        if (isGate)
        {
            SetRandomGate(ref stepWalls, ref wallObjs);
        }

        for (int i = 0; i < wallObjs.Length; i++)
        {
            Quaternion quaternionRotate;
            if (isRotate)
            {
                startPositionWall.z += stepWalls[i] * 0.5f;
                quaternionRotate = Quaternion.Euler(0f, 90f, 0f);
            }
            else
            {
                startPositionWall.x += stepWalls[i] * 0.5f;
                quaternionRotate = Quaternion.identity;
            }
            var obj = Instantiate(wallObjs[i], startPositionWall, quaternionRotate, objWall.transform);
            obj.name = obj.name.Replace("(Clone)", "") + " " + id;
            id++;
            if (isRotate)
            {
                startPositionWall.z += stepWalls[i] * 0.5f;
            }
            else
            {
                startPositionWall.x += stepWalls[i] * 0.5f;
            }
        }
    }

    private void SetRandomGate(ref List<float> steps, ref GameObject[] prefabsWall)
    {
        var random = Random.Range(0, steps.Count);
        steps[0] = steps[random];
        steps[random] = stepGate;
        prefabsWall[0] = prefabsWall[random];
        prefabsWall[random] = prefabGate[Random.Range(0, prefabGate.Count)];

    }
    private GameObject[] GetGameObjects(bool isGate, float length, out List<float> stepWalls)
    {
        float[] stepWall = new float[3] { stepWallSmall, stepWallMedium, stepWallBig };
        float[] lengthWall = new float[10];
        List<List<float>> variants = new List<List<float>>();
        if (isGate)
        {
            for (int i = 0; i < lengthWall.Length; i++)
            {
                lengthWall[i] += 5.5f;
            }
        }
        for (int i = 0; i < lengthWall.Length; i++)
        {
            List<float> variant = new List<float>();
            if (isGate)
            {
                variant.Add(stepWall[2]);
            }
            while (lengthWall[i] < length)
            {
                var random = Random.Range(0, 3);
                lengthWall[i] += stepWall[random];
                variant.Add(stepWall[random]);
            }

            variants.Add(variant);
        }

        float lengthWallMin = lengthWall[0];
        int number = 0;
        for (int i = 1; i < lengthWall.Length; i++)
        {
            if (lengthWallMin > lengthWall[i])
            {
                lengthWallMin = lengthWall[i];
                number = i;
            }
        }
        GameObject[] prefabs = new GameObject[variants[number].Count];
        for (int i = 0; i < variants[number].Count; i++)
        {
            if (variants[number][i] - 3.5f < 0.3f)
            {
                prefabs[i] = prefabsWallSmall[Random.Range(0, prefabsWallSmall.Count)];
                continue;
            }
            if (variants[number][i] - 4f < 0.3f)
            {
                prefabs[i] = prefabsWallMedium[Random.Range(0, prefabsWallMedium.Count)];
                continue;
            }
            if (variants[number][i] - 5.5f < 0.3f)
            {
                prefabs[i] = prefabsWallBig[Random.Range(0, prefabsWallBig.Count)];
                continue;
            }
        }
        stepWalls = variants[number];
        return prefabs;

    }

    private Vector3[] GetCornerPositionsAndScale()
    {
        Vector3[] positionsAndScale = new Vector3[8];
        positionsAndScale[0] = new Vector3(startPosition.x - stepFloor * 0.5f, startPosition.y, startPosition.z - stepFloor * 0.5f + stepCornerOffset);
        positionsAndScale[1] = new Vector3(startPosition.x - stepFloor * 0.5f, startPosition.y, startPosition.z + stepFloor * (length - 0.5f) - stepCornerOffset);
        positionsAndScale[2] = new Vector3(startPosition.x + stepFloor * (width - 0.5f), startPosition.y, startPosition.z + stepFloor * (length - 0.5f) - stepCornerOffset);
        positionsAndScale[3] = new Vector3(startPosition.x + stepFloor * (width - 0.5f), startPosition.y, startPosition.z - stepFloor * 0.5f + stepCornerOffset);
        positionsAndScale[4] = new Vector3(1f, 1f, -1f);
        positionsAndScale[5] = new Vector3(1f, 1f, 1f);
        positionsAndScale[6] = new Vector3(-1f, 1f, 1f);
        positionsAndScale[7] = new Vector3(-1f, 1f, -1f);
        return positionsAndScale;
    }
}
