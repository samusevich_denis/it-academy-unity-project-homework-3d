﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class Datas
{

    public List<GameObjectDatas> database;

}
[System.Serializable]
public class GameObjectDatas
{
    [System.Serializable]
    public class LevelObjectData
    {
        public string nameObject;
        public GameObject ObjectPrefabs;
        public Vector3 positionsObject;
        public Quaternion quaternionObject;
        public Vector3 scaleObject;
        public string transformNameParent;


        public LevelObjectData(string name, GameObject prefabs, Vector3 position, Quaternion quaternion, Vector3 scale, string transformName)
        {
            nameObject = name;
            ObjectPrefabs = prefabs;
            positionsObject = position;
            quaternionObject = quaternion;
            scaleObject = scale;
            transformNameParent = transformName;
        }
    }
    public string nameData;
    public LevelObjectData[] levelObjectDatas;
}
public class LevelObjectEditor : EditorWindow
{
    private string projectObjectDataPath = "/Data/GameObjectDatas.json";
    private string projectObjectPath = "LevelObjects";
    private int numberActiveData = -1;
    private string nameActiveData;
    private Vector2 scrolPosition = Vector2.zero;
    private GameObject[] prefabs;

    public Datas gameObjectDatas;
    public GameObjectDatas activeGameObjectDatas;


    [MenuItem("Tools/Level Object Editor")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(LevelObjectEditor)).Show();
    }

    private void Awake()
    {
        var objs = Resources.LoadAll(projectObjectPath);
        prefabs = new GameObject[objs.Length];
        for (int i = 0; i < objs.Length; i++)
        {
            prefabs[i] = (GameObject)objs[i];
        }
        LoadFileData();
        LoadSceneData();
    }

    private void OnGUI()
    {
        scrolPosition = GUILayout.BeginScrollView(scrolPosition, GUIStyle.none);
        SerializedObject serializedObject = new SerializedObject(this);
        var serializedProperty = serializedObject.FindProperty("gameObjectDatas");
        var serializedArray = serializedProperty.FindPropertyRelative("database");

        GUILayout.Label($"{gameObjectDatas.database.Count} Data found in file");
        for (int i = 0; i < gameObjectDatas.database.Count; i++)
        {
            GUILayout.Space(20);
            GUILayout.Label($"Data objects №{i}  \"{gameObjectDatas.database[i].nameData} \"");
            EditorGUILayout.PropertyField(serializedArray.GetArrayElementAtIndex(i), true);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button($"Load data №{i} in scene"))
            {
                LoadGameDataInScene(i);
            }
            if (GUILayout.Button($"Delete data №{i} "))
            {
                DeleteGameData(i);
            }
            GUILayout.EndHorizontal();
        }
        GUILayout.Space(25);
        var serializedActiveProperty = serializedObject.FindProperty("activeGameObjectDatas");
        EditorGUILayout.PropertyField(serializedActiveProperty, true);

        GUILayout.BeginHorizontal();
        var num = numberActiveData < 0 ? "\"empty\"" : numberActiveData.ToString();
        GUILayout.Label($"Name active data №" + num+ " "+" "+nameActiveData);
        nameActiveData = GUILayout.TextField(nameActiveData, GUILayout.Width(100));
        var text = numberActiveData >= 0 ? "" : "Enter name";
        GUILayout.Label(text);
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Save Data"))
        {
            if (numberActiveData < 0)
            {
                RefreshDataScene();
                SaveNewData(nameActiveData);
            }
            else
            {
                RefreshDataScene();
                SaveActiveData(numberActiveData, nameActiveData);
            }

        }
        if (GUILayout.Button("Load new data from the scene "))
        {
            LoadNewDataScene();
        }
        GUILayout.EndScrollView();
    }

    private void LoadFileData()
    {
        string filePath = $"{Application.dataPath}{projectObjectDataPath}";
        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            gameObjectDatas = JsonUtility.FromJson<Datas>(dataAsJson);
            if (gameObjectDatas.database == null)
            {
                gameObjectDatas.database = new List<GameObjectDatas>();
            }
        }
        else
        {
            gameObjectDatas = new Datas();
            gameObjectDatas.database = new List<GameObjectDatas>();
        }
    }
    private void LoadSceneData()
    {
        activeGameObjectDatas = new GameObjectDatas();
        activeGameObjectDatas.nameData = nameActiveData != null ? nameActiveData : string.Empty;
        var allObjectsTransform = FindObjectsOfType<Transform>();

        activeGameObjectDatas.levelObjectDatas = new GameObjectDatas.LevelObjectData[allObjectsTransform.Length];

        for (int i = 0; i < activeGameObjectDatas.levelObjectDatas.Length; i++)
        {
            string parentName;
            var parent = allObjectsTransform[i].parent;
            if (parent == null)
            {
                parentName = string.Empty;
            }
            else
            {
                parentName = allObjectsTransform[i].parent.gameObject.name;
            }
            int count = allObjectsTransform[i].gameObject.name.IndexOf(' ');
            string findNamePrefab;
            if (count < 0)
            {

                findNamePrefab = allObjectsTransform[i].gameObject.name;
            }
            else
            {
                findNamePrefab = allObjectsTransform[i].gameObject.name.Substring(0, count);
            }
            var objectPrefab = prefabs.FirstOrDefault<GameObject>(a => a.name.Equals(findNamePrefab, System.StringComparison.Ordinal));

            activeGameObjectDatas.levelObjectDatas[i] = new GameObjectDatas.LevelObjectData(
                allObjectsTransform[i].gameObject.name,
                objectPrefab,
                allObjectsTransform[i].position,
                allObjectsTransform[i].rotation,
                allObjectsTransform[i].localScale,
                parentName);
        }
    }

    private void RefreshDataScene()
    {
        LoadSceneData();
    }
    private void LoadNewDataScene()
    {
        numberActiveData = -1;
        nameActiveData = null;
        LoadSceneData();
    }
    private void DeleteGameData(int index)
    {

        gameObjectDatas.database.RemoveAt(index);
        SaveFileData();
        if (index == numberActiveData)
        {
            numberActiveData = -1;
            nameActiveData = null;
            LoadSceneData();
        }
        if (numberActiveData >index)
        {
            numberActiveData -= 1;
        }




    }
    private void DeleteObjectInScene()
    {
        for (int i = 0; i < activeGameObjectDatas.levelObjectDatas.Length; i++)
        {
            var obj = FindObjectOfType<Transform>();
            if (obj != null)
            {
                DestroyImmediate(obj.gameObject);
            }
            else
            {
                break;
            }
        }
    }

    private void LoadGameDataInScene(int index)
    {
        if (numberActiveData>=0)
        {
            RefreshDataScene();
            SaveActiveData(numberActiveData, activeGameObjectDatas.nameData);
        }

        DeleteObjectInScene();
        InstantiateObjectInScene(index);

        nameActiveData = gameObjectDatas.database[index].nameData;
        numberActiveData = index;

    }
    private void InstantiateObjectInScene(int index)
    {
        for (int i = 0; i < gameObjectDatas.database[index].levelObjectDatas.Length; i++)
        {
            GameObject prefab;
            GameObject obj;
            if (gameObjectDatas.database[index].levelObjectDatas[i].ObjectPrefabs == null)
            {
                prefab = null;
            }
            else
            {
                prefab = GetPrefab(gameObjectDatas.database[index].levelObjectDatas[i].ObjectPrefabs.name);
            }


            if (prefab == null)
            {
                obj = new GameObject();
            }
            else
            {
                obj = Instantiate(prefab);
            }

            obj.name = gameObjectDatas.database[index].levelObjectDatas[i].nameObject;
            obj.transform.position = gameObjectDatas.database[index].levelObjectDatas[i].positionsObject;
            obj.transform.rotation = gameObjectDatas.database[index].levelObjectDatas[i].quaternionObject;
            obj.transform.localScale = gameObjectDatas.database[index].levelObjectDatas[i].scaleObject;
        }

        for (int i = 0; i < gameObjectDatas.database[index].levelObjectDatas.Length; i++)
        {
            if (gameObjectDatas.database[index].levelObjectDatas[i].transformNameParent.Equals(string.Empty, System.StringComparison.OrdinalIgnoreCase))
            {
                continue;
            }
            else
            {
                var allObject = FindObjectsOfType<Transform>();
                var parentObject = FindTransform(allObject, gameObjectDatas.database[index].levelObjectDatas[i].transformNameParent);/*.FirstOrDefault<Transform>(n => n.gameObject.name.Equals(gameObjectDatas.database[index].levelObjectDatas[i].transformNameParent, System.StringComparison.OrdinalIgnoreCase))*/
                var childObject = FindTransform(allObject, gameObjectDatas.database[index].levelObjectDatas[i].nameObject);//.FirstOrDefault<Transform>(n => n.gameObject.name.Equals(gameObjectDatas.database[index].levelObjectDatas[i].nameObject, System.StringComparison.OrdinalIgnoreCase));
                childObject.parent = parentObject;
            }
        }
    }

    private void SaveActiveData(int index, string name)
    {
        activeGameObjectDatas.nameData = name;
        gameObjectDatas.database[index] = activeGameObjectDatas;
        SaveFileData();
        LoadFileData();
    }
    private void SaveNewData(string name)
    {
        activeGameObjectDatas.nameData = name;
        gameObjectDatas.database.Add(activeGameObjectDatas);
        SaveFileData();
        LoadFileData();
        numberActiveData = gameObjectDatas.database.Count - 1;
        //LoadGameDataInScene(gameObjectDatas.database.Count - 1);
    }
    private void SaveFileData()
    {
        string dataAsJson = JsonUtility.ToJson(gameObjectDatas);
        var filePath = $"{Application.dataPath}{projectObjectDataPath}";

        File.WriteAllText(filePath, dataAsJson);
        AssetDatabase.Refresh();
    }

    private GameObject GetPrefab(string name)
    {
        for (int i = 0; i < prefabs.Length; i++)
        {
            if (prefabs[i].name.Equals(name,System.StringComparison.Ordinal))
            {
                return prefabs[i];
            }
        }
        return null;
    }
    private Transform FindTransform(Transform[] transforms, string nameGameObject)
    {
        for (int i = 0; i < transforms.Length; i++)
        {
            if (nameGameObject.Equals(transforms[i].gameObject.name, System.StringComparison.Ordinal))//&&transforms[i].parent==null)
            {
                return transforms[i];
            }
        }
        return null;
    }
}