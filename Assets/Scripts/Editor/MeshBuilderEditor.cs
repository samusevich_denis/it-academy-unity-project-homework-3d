﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.XR;

public delegate void MethodData();
public class MeshBuilderEditor : EditorWindow
{

    private static SerializableMeshInfo meshInfo;
    private const string LevelDataPath = "Assets/Data/LevelsData/";
    
    [MenuItem("Tools/Mesh/SaveMesh")]
    private static void SaveSelectedMesh()
    {
        var obj = Selection.activeGameObject;
        if (obj==null)
        {
            return;
        }

        var meshFilter = obj.GetComponent<MeshFilter>();
        if (meshFilter==null)
        {
            return;
        }
        
        var meshRenderer = obj.GetComponent<MeshRenderer>();
        if (meshRenderer==null)
        {
            return;
        }
        
        meshInfo = new SerializableMeshInfo(obj.name, meshFilter.sharedMesh, meshRenderer.sharedMaterial, obj.transform);

        var json = JsonUtility.ToJson(meshInfo);
        //Debug.Log(json);
        PlayerPrefs.SetString("MeshInfo",JsonUtility.ToJson(meshInfo));
    }

    [MenuItem("Tools/Mesh/LoadMesh")]
    private static void LoadMesh()
    {

        if (meshInfo == null && PlayerPrefs.HasKey("MeshInfo"))
        {
            Debug.Log("Loaded from Player Prefs");
            var json = PlayerPrefs.GetString("MeshInfo");
            meshInfo = JsonUtility.FromJson<SerializableMeshInfo>(json);
        }

        if (meshInfo == null)
        {
            Debug.LogError("No mesh data");
            return;
        }

        var obj = Selection.activeGameObject;
        meshInfo.BuildObject(obj != null ? obj.transform : null);
    }

    [MenuItem("Tools/Mesh/Save to Data")]
    private static void SaveCheck()

    {
        if (isFoundData())
        {
            WindowLog.ShowWindow("Found an old file in the database, overwrite?", SaveSelectedToData);
        }
        else
        {
            SaveSelectedToData();
        }
    }
    private static void DeleteData()
    {
        var name = "Level_test_";
        var assetPath = $"{LevelDataPath}{name}{typeof(LevelMeshData)}.asset";
        AssetDatabase.DeleteAsset(assetPath);
        AssetDatabase.Refresh();
    }
    private static void SaveSelectedToData()
    {
        if (isFoundData())
        {
            DeleteData();
        }
        var obj = Selection.activeGameObject;
        if (obj == null)
        {
            Debug.LogError("No object in selection");
            return;
        }

        var data = CreateAssets<LevelMeshData>("Level_test_");
        data.SetupData(obj);

    }
    private static bool isFoundData()
    {
        var name = "/Data/LevelsData/Level_test_";
        var assetPath = Application.dataPath+ $"{name}{typeof(LevelMeshData)}.asset";
        return File.Exists(assetPath);
    }

    [MenuItem("Tools/Mesh/Load from Data")]
    private static void LoadMeshesFromSelectedData()
    {
        var name = "Level_test_";
        var assetPath = $"{LevelDataPath}{name}{typeof(LevelMeshData)}.asset";
        var data = AssetDatabase.LoadAssetAtPath(assetPath, typeof(LevelMeshData)) as LevelMeshData;
        if (data == null)
        {
            return;
        }

        var obj = Selection.activeGameObject;
        foreach (var info in data.MeshInfos)
        {
            info.BuildObject(obj != null ? obj.transform : null);
        }
    }



    private static T CreateAssets<T>(string name = "") where T : ScriptableObject
    {
        var assets = CreateInstance<T>();
        var assetPath = $"{LevelDataPath}{name}{typeof(T)}.asset";
        AssetDatabase.CreateAsset(assets, assetPath);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = assets;
        return assets;
    }
}
