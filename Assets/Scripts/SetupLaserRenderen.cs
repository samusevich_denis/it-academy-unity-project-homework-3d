﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetupLaserRenderen : MonoBehaviour
{
    private LineRenderer laser;
    void Start()
    {
        laser = GetComponent<LineRenderer>();
        laser.enabled = false;
    }

    public void SetActiveLaser(bool isActive)
    {
        laser.enabled = isActive;
    }
    public void SetTargetLaser(Vector3 positionTarget)
    {
        laser.SetPositions(new Vector3[2] { transform.position, positionTarget });
    }
}
