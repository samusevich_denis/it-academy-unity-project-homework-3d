﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour
{
    [SerializeField] private Vector3 vector;
    [SerializeField] private Vector3 vector2;
    [SerializeField] private float posMinY;
    [SerializeField] private float posMaxY;
    [SerializeField] private float angle;
    [SerializeField] private Transform transform;

    [SerializeField] private Transform obj1;
    [SerializeField] private Transform obj2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        obj1.position = vector;
        vector2 = RotateVector(vector, (Mathf.PI * 0.5f * (vector.y - posMinY) / (posMaxY - posMinY)), transform);
        obj2.position = vector2;
        angle = Mathf.PI * 0.5f * (vector.y - posMinY) / (posMaxY - posMinY);
    }

    private Vector3 RotateVector(Vector3 positionWorldSpace, float radian, Transform rotatePoint)
    {
        var posY = positionWorldSpace.y;
        var localPosition = rotatePoint.InverseTransformPoint(positionWorldSpace);
        localPosition.y = 0f;
        Vector2 localPositionXZ = new Vector2(localPosition.x, localPosition.z);
        var startRadian = Mathf.Acos(localPositionXZ.x / localPositionXZ.magnitude);
        var targetRadian = startRadian + radian;
        Vector3 localRotatePositionMax = new Vector3(localPositionXZ.magnitude*Mathf.Cos(targetRadian), 0f, localPositionXZ.magnitude * Mathf.Sin(targetRadian));
        var vectorReturn = rotatePoint.TransformPoint(localRotatePositionMax);
        vectorReturn.y = posY;
        return vectorReturn;
    }
}
