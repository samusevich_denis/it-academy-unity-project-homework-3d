﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsPool : SingletonObject<ObjectsPool>
{

    
    private  Dictionary<GameObject, List<GameObject>> pool = new Dictionary<GameObject, List<GameObject>>();

    public GameObject GetObject(GameObject prefab)
    {
        if (!pool.ContainsKey(prefab))
        {
            var obj = Object.Instantiate(prefab);
            pool[prefab] = new List<GameObject>{obj};
            obj.SetActive(true);
            return obj;
        }

        var objects = pool[prefab];
        foreach (var obj in objects)
        {
            if (obj !=null &&!obj.activeSelf)
            {
                obj.SetActive(true);
                return obj;
            }
        }

        var newObj = Object.Instantiate(prefab);
        newObj.SetActive(true);
        objects.Add(newObj);
        return newObj;
    }

    public GameObject GetObject(GameObject prefab, Vector3 position, Quaternion rotation)
    {
        var obj = GetObject(prefab);
        obj.transform.position = position;
        obj.transform.rotation = rotation;
        return obj;
    }
    public void AddObjects(GameObject prefab, int count)
    {
        var objects = new List<GameObject>();
        for (int i = 0; i < count; i++)
        {
            var obj = Object.Instantiate(prefab);
            obj.SetActive(false);
            objects.Add(obj);
        }

        if (!pool.ContainsKey(prefab))
        {
            pool[prefab] = objects;
        }
        else
        {
            pool[prefab].AddRange(objects);
        }
    }
}
