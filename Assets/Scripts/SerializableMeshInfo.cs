﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SerializableMeshInfo
{
    public string name;

    public Material material;

    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uv;
    public Vector2[] uv2;
    public Vector3[] normals;
    public Color[] colors;
    public Vector3 position;
    public Quaternion rotate;
    public Vector3 localScale;

    public SerializableMeshInfo(string name, Mesh mesh, Material material, Transform transform)
    {
        this.name = name;

        this.material = material;

        vertices = mesh.vertices;
        triangles = mesh.triangles;
        uv = mesh.uv;
        uv2 = mesh.uv2;
        normals = mesh.normals;
        colors = mesh.colors;
        position = transform.position;
        rotate = transform.rotation;
        localScale = transform.localScale;
    }

    public GameObject BuildObject(Transform parent = null)
    {
        var obj = new GameObject($"{name} clone");
        if (parent)
        {
            obj.transform.SetParent(parent);
        }

        var meshFilter = obj.AddComponent<MeshFilter>();
        var meshRenderer = obj.AddComponent<MeshRenderer>();
        meshFilter.sharedMesh = GetMesh();
        meshRenderer.sharedMaterial = material;
        obj.transform.position = position;
        obj.transform.rotation = rotate;
        obj.transform.localScale = localScale;
        return obj;
    }

    private Mesh GetMesh()
    {
        var mesh = new Mesh
        {
            vertices = vertices,
            triangles = triangles,
            uv = uv,
            uv2 = uv2,
            normals = normals,
            colors = colors,
        };
        return mesh;
    }
}
